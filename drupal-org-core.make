; A separate drupal-org-core.make file makes it so we can apply core patches
; if we need to.

api = 2
core = 7.x
projects[drupal][type] = core
projects[drupal][version] = 7.21

; CORE PATCHES
; Add "exclusive" property to install profiles to auto-select them.
; @todo: Part of core as of 7.22, so remove whenever that comes out.
projects[drupal][patch][1727430] = http://drupal.org/files/drupal-provide_exclusive_property_install_profiles-1727430-35-d7.patch

