api = 2
core = 7.x
; Include the definition for how to build Drupal core directly, including patches:
includes[] = drupal-org-core.make
; Download the Folders install profile and recursively build all its dependencies:
projects[folders][type] = profile
projects[folders][download][type] = git
projects[folders][download][branch] = 7.x-1.x

