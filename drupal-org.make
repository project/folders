core = 7.x
api = 2

; Modules
projects[multiupload_filefield_widget][type] = module
projects[multiupload_filefield_widget][version] = 1.0
projects[multiupload_filefield_widget][subdir] = contrib

; Themes
projects[zen][type] = theme
projects[zen][version] = 5.1

